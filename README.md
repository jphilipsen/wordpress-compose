# WordPress Compose
This repository allows you build a local wordpress instance using the data in your home folder.

## Video Description

https://youtu.be/mENjJzFakWQ

## Compose WordPress
1. Pull the repository to your local directory: 
    ``` bash
    git clone https://gitlab.com/jphilipsen/wordpress-compose.git
    ```
2. Change the .env and make the PWD parameter point to your home directory
3. In the root directory run:
    ```bash
    docker-compose up
    ```
4. If you would like to run it in the background then run:
    ```bash
    docker-compose up -d
    ```
5. To connect to your wordpress instance go to a web browser and type in [localhost:8080](localhost:8080)
6. When you are finished make sure to run the below command to close our your docker container or if you ran the
command in step 3 then just crtl+c out of the command. Otherwise:
    ```bash
    docker-compose down
    ```


## Requirements
The software needed on your machine to run this is:
* docker
* docker-compose

To see more about the docker images you can see this [repo](https://hub.docker.com/r/bitnami/wordpress)
